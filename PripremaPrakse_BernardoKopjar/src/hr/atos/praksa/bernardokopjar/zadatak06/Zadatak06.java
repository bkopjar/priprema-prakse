package hr.atos.praksa.bernardokopjar.zadatak06;

import java.util.Scanner;

public class Zadatak06 {
	
	public static void main (String args []) {
		
		System.out.print("Unesi ime: ");
		Scanner unos = new Scanner(System.in);
		String ime = unos.next();
		int i, j, k;
		String potpis = ":  :  :  :  :  :  :  :  :  :  :";
		
		for (i = 0; i < 31; i++) {
			System.out.print("-");
		}
		System.out.println();
		System.out.println(": : :  TABLICA  MNOZENJA  : : :");
		for (i = 0; i < 31; i++) {
			System.out.print("-");
		}
		System.out.println();
		
		System.out.print(" * |");
		for (j = 1; j <= 9; j++){
			System.out.printf("%3d",j);
		}
		System.out.println();
		
		for (i = 0; i < 31; i++) {
			System.out.print("-");
		}
		System.out.println();
		
		for(i = 1; i <= 9; i++){
			System.out.printf("%2d |",i);
			for(j = 1; j <= 9; j++){
				System.out.printf("%3d",i*j);
			}
			System.out.println();
		}
		
		for (i = 0; i < 31; i++) {
			System.out.print("-");
		}
		System.out.println();
		
		System.out.println(potpis.substring(0, 31-ime.length()) + ime);
		
		for (i = 0; i < 31; i++) {
			System.out.print("-");
		}
		System.out.println();
		unos.close();
	}

}
