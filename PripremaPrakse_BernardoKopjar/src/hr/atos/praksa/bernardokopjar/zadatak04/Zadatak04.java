package hr.atos.praksa.bernardokopjar.zadatak04;

public class Zadatak04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int[] polje = {5, 11 ,22, -6, 13 };
		
		for (int i = 0; i < polje.length; i++) {
			
			if (polje[i] % 2 == 0) {
				System.out.println(polje[i] + " je paran broj");
			}
			else {
				System.out.println(polje[i] + " je neparan broj");
			}
		}
	}

}
