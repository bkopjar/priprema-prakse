package hr.atos.praksa.bernardokopjar.zadatak12;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Zadatak12 {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		File direktorij = new File("E:\\Faks\\Eclipse\\workspace\\PripremaPrakse_BernardoKopjar\\src\\hr\\atos\\praksa\\bernardokopjar\\zadatak12\\Datoteke");
	    FilenameFilter filter = new FilenameFilter() {
	       public boolean accept (File dir, String name) { 
	          return name.endsWith(".txt");
	       } 
	    }; 
	    String[] priv = direktorij.list(filter);
	    if (priv == null) {
	    	System.out.println("Greska"); 
	    } else {
	    
	    	for (int i = 0; i< priv.length; i++) {
	    		System.out.println((i+1)+". "+priv[i]);
	    	}
	    }
	    System.out.println();
		
		
		Scanner datoteka = new Scanner(System.in);
		System.out.print("Odaberi broj datoteke: ");
		int broj=datoteka.nextInt();
		
		
		System.out.println("U datoteci "+priv[broj-1]+" nalaze se sljede�e rijeci: ");
		
		for(int i=0;i<24;i++) {
			System.out.print("-");
		}
		System.out.println();
		
		System.out.println("Rijec (broj ponavljanja)");
		for(int i=0;i<24;i++) {
			System.out.print("-");
		}
		
		
		ArrayList<String> rijeci= new ArrayList<String>();
		ArrayList<Integer> brojac= new ArrayList<Integer>();
		
		FileInputStream txtDatoteka=new FileInputStream(direktorij+"\\"+priv[broj-1]);
		Scanner zaDatoteku=new Scanner(txtDatoteka);
		while(zaDatoteku.hasNext()) {
			String sljedecaRijec = zaDatoteku.next().toLowerCase();
			if(rijeci.contains(sljedecaRijec)) {
				int index= rijeci.indexOf(sljedecaRijec);
				brojac.set(index, brojac.get(index)+1);
			}else {
				rijeci.add(sljedecaRijec);
				brojac.add(1);
			}
		}
		zaDatoteku.close();
		txtDatoteka.close();
		datoteka.close();
		
		Collections.sort(rijeci);
		
		System.out.println();
		for(int i=0;i<rijeci.size();i++) {
			System.out.println(rijeci.get(i)+" ("+brojac.get(i)+")");
		}
		
		
		for(int i=0;i<24;i++) {
			System.out.print("-");
		}
		
	}


}


