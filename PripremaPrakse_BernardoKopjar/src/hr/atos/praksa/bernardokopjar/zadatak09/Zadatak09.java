package hr.atos.praksa.bernardokopjar.zadatak09;
import java.util.Scanner;



public class Zadatak09 {
	
	public static void main(String[] args) {

	Scanner unos = new Scanner(System.in);
	
	String [][] matrica=new String[8][13];
	int[] mjesec=new int[13];
	
	int placa=0;
	int mjesec2=1;
	int privremena=5000;
	
	for(int i=1;i<mjesec.length;i++) {
		System.out.print("Unesi placu za "+i+". mjesec: ");
		placa=unos.nextInt();
		mjesec[i]=placa;
	}
	System.out.println();
	
	
	for(int i=0;i<matrica.length;i++) {
		for(int j=0;j<matrica[i].length;j++) {
			matrica[i][j]="";
			
			if(j==0 && i<=6 || j!=0 && i<=6 || i<=7) {
				
				if(j==0 && i<6) {
					if(privremena!=0) {
						matrica[i][0]=Integer.toString(privremena);
						System.out.print(matrica[i][0]+"kn - |");
						privremena-=1000;
					}else if(privremena==0) {
						matrica[i][j]="0";
						System.out.printf("%4skn - |",matrica[i][j]);
						
					}
				}
				//dodavanje X znaka
				if(mjesec[j]<5001 && mjesec[j]>4500 ) {
			
					matrica[0][j]="x";
					
				}
				if(mjesec[j]<4501 && mjesec[j]>3500) {
					matrica[1][j]="x";
					
				} 
				if(mjesec[j]<3501 && mjesec[j]>2500) {
					matrica[2][j]="x";
				
				}
				if(mjesec[j]<2501 && mjesec[j]>1500) {
					matrica[3][j]="x";
				
				}
				if(mjesec[j]<1501 && mjesec[j]>500) {
					matrica[4][j]="x";
					
				}
				if(mjesec[j]<501 && mjesec[j]>0) {
					matrica[5][j]="x";
				
				}
				
				if(j!=0 && i<6) {
					System.out.printf("%2s ",matrica[i][j]);
					if(i<5 && j==12) {
						System.out.println();
						System.out.println("         |");
					}
				}
				
				if(j==0 && i==6) {
					matrica[i][j]="";
					System.out.printf("\n%10s",matrica[i][j]);
				}
				
				if(j!=0 && i==6 || i==7) {
					if(j!=0 && i==6) {
						matrica[i][j]="-- ";
						System.out.print(matrica[i][j]);
						if(j==12) {
							System.out.println();
							matrica[i][j]="";
							System.out.printf("%10s",matrica[i][j]);
						}
					}else if(j!=0 && i==7) {
						matrica[i][j]=Integer.toString(mjesec2);
						System.out.printf("%2s ",matrica[i][j]);
						mjesec2++;
					}
				}
			}
		}
	}

	unos.close();
	}
	}
