package hr.atos.praksa.bernardokopjar.zadatak05;

import java.util.Scanner;

public class Zadatak05 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int brojac = 0;
		Scanner unos = new Scanner(System.in);
		System.out.print("Pocetak intervala: ");
		int pocetak = unos.nextInt();
		System.out.print("Kraj intervala: ");
		int kraj = unos.nextInt();
		
		if(pocetak > 10 && kraj < 100) {
			System.out.println("Pogreska! Pocetak intervala mora biti manji od 10 a kraj veci od 100");
		}
		else {
			for (int broj=pocetak; broj <= kraj; broj++) {
				if(broj <= 15) {
					brojac += 5;
					System.out.println(brojac);
				}
				if(broj > 15 && broj < 20) {
					brojac -= 1;
					System.out.println(brojac);
				}
				if(broj % 20 == 0) {
					continue;
				}
				if (broj == 75) {
					System.out.println("Vrijednost brojaca je "+brojac);
					break;
				}
		}

	}
		unos.close();

	}
}
