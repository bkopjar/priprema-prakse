package hr.atos.praksa.bernardokopjar.zadatak07;

import java.util.Scanner;

public class Zadatak07 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner input = new Scanner(System.in);

        System.out.println("Unesi pocetak i kraj intervala(dg gg):");
        int dg = input.nextInt();
        int gg = input.nextInt();
        int br=0;
        int razlika = gg - dg;
        int ostatak = dg % 7;
        if(ostatak==0)br++;

        for(int i=7-ostatak;i<=razlika;i=i+7) {
            br++;
        }

        System.out.printf("Izme�u %d i %d ima %d brojeva djeljivih sa 7.", dg, gg, br);

        input.close();
	}
}
