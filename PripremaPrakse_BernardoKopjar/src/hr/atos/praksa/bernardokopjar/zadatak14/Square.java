package hr.atos.praksa.bernardokopjar.zadatak14;

public class Square extends Result {
	
	private double side;
	
	public Square() {
		this.side = 0;
	}
	
	public Square(double side) {
		this.side = side;
	}


	public double getSide() {
		return side;
	}

	public void setSide(double side) {
		this.side = side;
	}
	
	

}
