package hr.atos.praksa.bernardokopjar.zadatak14;

public interface Diagonal {
	abstract  public void getDiagonal(double firstSide, double secondSide);
}
