package hr.atos.praksa.bernardokopjar.zadatak14;

public class Rectangle extends Result {

	private double firstSide;
	private double secondSide;
	
	public Rectangle() {
		this.firstSide = 0;
		this.secondSide = 0;
	}
	
	public Rectangle(double firstSide,double secondSide) {
		this.firstSide = firstSide;
		this.secondSide = secondSide;
	}

	public double getFirstSide() {
		return firstSide;
	}

	public void setFirstSide(double firstSide) {
		this.firstSide = firstSide;
	}

	public double getSecondSide() {
		return secondSide;
	}

	public void setSecondSide(double secondSide) {
		this.secondSide = secondSide;
	}
}
