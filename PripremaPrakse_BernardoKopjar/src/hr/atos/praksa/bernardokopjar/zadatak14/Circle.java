package hr.atos.praksa.bernardokopjar.zadatak14;

public class Circle extends Result {
	
private double radius;
	
	public Circle() {
		this.radius = 0;
	}
	
	public Circle(double radius) {
		this.radius = radius;
	}
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}

}
